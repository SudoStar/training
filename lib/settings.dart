import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'main.dart';
import 'number_picker.dart';

class Settings {
  final int numRounds;
  final int numSprints;
  final int sprintTime;
  final int sprintPause;
  final int roundPause;

  const Settings(this.numRounds, this.numSprints, this.sprintTime,
      this.sprintPause, this.roundPause);
}

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  var _numRounds;
  var _numSprints;
  var _sprintTime;
  var _sprintPause;
  var _roundPause;

  void _route() {
    final settings = Settings(
        _numRounds, _numSprints, _sprintTime, _sprintPause, _roundPause);
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => MainPage(settings)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                NumberPickerWidget(
                  minValue: 1,
                  maxValue: 10,
                  step: 1,
                  displayText: "Rounds",
                  icon: Icons.repeat_rounded,
                  boxColor: Colors.blue.shade100,
                  onValueChanged: (value) => _numRounds = value,
                ),
                NumberPickerWidget(
                  minValue: 1,
                  maxValue: 10,
                  step: 1,
                  displayText: "Sprints per Round",
                  icon: Icons.directions_run_rounded,
                  boxColor: Colors.green.shade100,
                  onValueChanged: (value) => _numSprints = value,
                ),
                NumberPickerWidget(
                  minValue: 10,
                  maxValue: 120,
                  step: 10,
                  displayText: "Seconds per Sprint",
                  icon: Icons.timelapse,
                  boxColor: Colors.orange.shade100,
                  onValueChanged: (value) => _sprintTime = value,
                ),
                NumberPickerWidget(
                  minValue: 0,
                  maxValue: 60,
                  step: 10,
                  displayText: "Pause between Sprints",
                  icon: Icons.free_breakfast_rounded,
                  boxColor: Colors.pink.shade100,
                  onValueChanged: (value) => _sprintPause = value,
                ),
                NumberPickerWidget(
                  minValue: 0,
                  maxValue: 120,
                  step: 15,
                  displayText: "Pause between Rounds",
                  icon: Icons.airline_seat_individual_suite_rounded,
                  boxColor: Colors.deepPurple.shade100,
                  onValueChanged: (value) => _roundPause = value,
                ),
              ],
            ),
            Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.blue.shade100, width: 4),
                    color: Colors.blue.shade100,
                    shape: BoxShape.circle),
                child: IconButton(
                  icon: Icon(Icons.check_rounded),
                  color: Colors.black,
                  onPressed: _route,
                ))
          ],
        ),
      ),
    );
  }
}
