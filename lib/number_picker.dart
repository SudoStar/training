import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MarginTextBox extends StatelessWidget {
  final String text;
  final IconData icon;

  const MarginTextBox(this.text, this.icon);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(left: 15),
        child: RichText(
          text: TextSpan(
            style: TextStyle(fontSize: 20, color: Colors.black54),
            children: [
              WidgetSpan(
                child: Icon(
                  icon,
                  size: 24,
                ),
              ),
              TextSpan(
                text: " " + text,
              ),
            ],
          ),
        ));
  }
}

typedef IntCallback = Function(int number);

class NumberPickerWidget extends StatefulWidget {
  final int minValue;
  final int maxValue;
  final int step;
  final String displayText;
  final Color boxColor;
  final IconData icon;

  final IntCallback onValueChanged;

  const NumberPickerWidget(
      {Key key,
      this.minValue,
      this.maxValue,
      this.step,
      this.displayText,
      this.icon,
      this.boxColor,
      this.onValueChanged})
      : super(key: key);

  @override
  _NumberPickerWidgetState createState() => _NumberPickerWidgetState();
}

class _NumberPickerWidgetState extends State<NumberPickerWidget> {
  int _currentValue;
  var values = List<Text>();

  @override
  void initState() {
    _currentValue = widget.minValue;
    widget.onValueChanged(_currentValue);

    for (var i = widget.minValue; i <= widget.maxValue; i += widget.step) {
      values.add(Text(i.toString()));
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: widget.boxColor,
          border: Border.all(width: 2.0, color: const Color(0xFFFFFFFF))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MarginTextBox(widget.displayText, widget.icon),
          Container(
            width: 75,
            height: 100,
            child: CupertinoPicker(
              itemExtent: 32,
              onSelectedItemChanged: (int index) => setState(
                () {
                  _currentValue = int.parse(values[index].data);
                  widget.onValueChanged(_currentValue);
                },
              ),
              children: values,
            ),
          ),
        ],
      ),
    );
  }
}
