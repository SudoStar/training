import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:training/settings.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
      title: 'Training',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SettingsPage(),
    );
  }
}

class StopWatch {
  Timer timer;
  var counter = 0;
  final timerInterval = Duration(seconds: 1);

  void stopTimer() {
    if (timer != null) {
      timer.cancel();
    }
  }

  void tick(_) {
    counter++;
  }

  void startTimer() {
    timer = Timer.periodic(timerInterval, tick);
  }
}

class MainPage extends StatefulWidget {
  final Settings settings;

  const MainPage(this.settings);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  Timer _timer;
  int _currentRound = 1;
  int _currentSprint = 1;
  int _currentTime = 0;
  bool _isPause = false;
  bool _isRoundPause = false;

  void stopTimer() {
    _timer.cancel();
  }

  void startTimer() {
    _timer = new Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        _currentTime++;

        if (_currentTime > widget.settings.sprintTime &&
            (!_isPause && !_isRoundPause)) {
          _currentTime = 1;
          if (_currentSprint + 1 > widget.settings.numSprints) {
            _isRoundPause = true;
          } else {
            _isPause = true;
          }
        }

        if (_isRoundPause && _currentTime > widget.settings.roundPause) {
          _currentRound++;
          _isRoundPause = false;
          _currentTime = 1;
          _currentSprint = 1;
        }

        if (_isPause && _currentTime > widget.settings.sprintPause) {
          _currentSprint++;
          _isPause = false;
          _currentTime = 1;
        }

        if (_isRoundPause && _currentRound + 1 > widget.settings.numRounds ||
            _currentRound > widget.settings.numRounds) {
          _timer.cancel();
          _currentRound = 1;
          _currentSprint = 1;
          _currentTime = 0;
          _isRoundPause = false;
          _isPause = false;
        }
      });
    });
  }

  @override
  initState() {
    startTimer();
    super.initState();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  void _toggleClock() {
    setState(() {
      if (_timer.isActive) {
        stopTimer();
      } else {
        startTimer();
      }
    });
  }

  double calculateProgress() {
    if (_isPause) {
      return (_currentTime / widget.settings.sprintPause);
    } else if (_isRoundPause) {
      return (_currentTime / widget.settings.roundPause);
    } else {
      return (_currentTime / widget.settings.sprintTime);
    }
  }

  static var sprintPauseColorLight = Colors.pink.shade100;
  static var sprintPauseColorDark = Colors.pink.shade400;
  static var roundPauseColorLight = Colors.deepPurple.shade100;
  static var roundPauseColorDark = Colors.deepPurple.shade400;
  static var standardColorLight = Colors.blue.shade100;
  static var standardColorDark = Colors.blue.shade400;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
                "${widget.settings.numRounds} Round${widget.settings.numRounds > 1 ? "s" : ""} // ${widget.settings.numSprints} Sprint${widget.settings.numSprints > 1 ? "s" : ""}",
                style: TextStyle(fontSize: 14.0, color: Colors.black54)),
            SizedBox(
              height: 20,
            ),
            Text(
              "Round #$_currentRound",
              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
            ),
            Text(
              _isPause
                  ? "Pause"
                  : _isRoundPause
                      ? "Next: Round #${_currentRound + 1}"
                      : "Sprint #$_currentSprint",
              style: TextStyle(
                  fontSize: 24.0,
                  color: _isPause
                      ? sprintPauseColorDark
                      : _isRoundPause
                          ? roundPauseColorDark
                          : standardColorDark),
            ),
            SizedBox(
              height: 40,
            ),
            new CircularPercentIndicator(
              radius: MediaQuery.of(context).size.width - 50,
              lineWidth: 30.0,
              percent: calculateProgress(),
              circularStrokeCap: CircularStrokeCap.round,
              center: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.baseline,
                    children: [
                      Text((_currentTime).toString(),
                          style: TextStyle(fontSize: 52.0)),
                      Text(
                        "s",
                        style: TextStyle(fontSize: 28.0),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: _isPause
                                ? sprintPauseColorLight
                                : _isRoundPause
                                    ? roundPauseColorLight
                                    : standardColorLight,
                            width: 4),
                        color: _isPause
                            ? sprintPauseColorLight
                            : _isRoundPause
                                ? roundPauseColorLight
                                : standardColorLight,
                        shape: BoxShape.circle),
                    child: IconButton(
                      icon: _timer.isActive
                          ? Icon(Icons.pause)
                          : Icon(Icons.play_arrow),
                      color: Colors.black,
                      onPressed: _toggleClock,
                    ),
                  ),
                ],
              ),
              backgroundColor: _isPause
                  ? sprintPauseColorLight
                  : _isRoundPause
                      ? roundPauseColorLight
                      : standardColorLight,
              progressColor: _isPause
                  ? sprintPauseColorDark
                  : _isRoundPause
                      ? roundPauseColorDark
                      : standardColorDark,
            ),
          ],
        ),
      ),
    );
  }
}
