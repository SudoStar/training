# sprints

### Intro

Simple, modern time-based Training App built with Flutter

### Features

* Specify Rounds consisting of one or multiple Sprints
* Set Sprint time
* Specify Pauses between Rounds and Sprints

### Example Configuration

3 Rounds / 5 Sprints / 20 s per Sprint / no Pause between Sprints / 60 s Pause between Rounds
